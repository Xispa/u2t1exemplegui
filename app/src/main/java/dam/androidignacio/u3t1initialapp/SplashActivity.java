package dam.androidignacio.u3t1initialapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class SplashActivity extends AppCompatActivity {

    private MediaPlayer sound;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        hideTitleBar();
        hideToolBar();

        Thread timer = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    sleep(1000);
                    sound = MediaPlayer.create(getApplicationContext(), R.raw.bells);
                    sound.start();
                    sleep(5000);
                } catch(InterruptedException e) {

                } finally {
                    startActivity(new Intent("dam.android.u3t1initialapp.STARTINGPOINT"));
                }
            }
        };

        // start thread
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sound.release();
        sound = null;
        finish();
    }

    private void hideTitleBar() {
        actionBar = getSupportActionBar();
        actionBar.hide();
    }

    private void hideToolBar(){
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }

}